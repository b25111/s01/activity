name = "Jose"
age = 38
occupation = "writer"
movie = "One more chance"
rating = 99.6

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {99.6} %") 

num1, num2, num3 = 25, 50, 100

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)